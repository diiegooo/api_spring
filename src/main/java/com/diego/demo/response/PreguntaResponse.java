package com.diego.demo.response;

import java.util.List;   

import com.diego.demo.modelo.Pregunta;

public class PreguntaResponse {

    private List<Pregunta> categoria;  

    public List<Pregunta> getCategoria() {
        return categoria;
    }

    public void setCategoria(List<Pregunta> categoria) {
        this.categoria = categoria;
    }
}