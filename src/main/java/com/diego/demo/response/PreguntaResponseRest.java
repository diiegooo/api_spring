package com.diego.demo.response;

public class PreguntaResponseRest extends ResponseRest {

    private PreguntaResponse preguntaResponse = new PreguntaResponse();

    public PreguntaResponse getPreguntaResponse() {
        return preguntaResponse;
    }

    public void setPreguntaResponse(PreguntaResponse preguntaResponse) {
        this.preguntaResponse = preguntaResponse;
    }
}
