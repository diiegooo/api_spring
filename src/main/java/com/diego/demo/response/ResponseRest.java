package com.diego.demo.response;


import java.util.ArrayList;    
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ResponseRest {

    private List<Map<String, String>> metadata = new ArrayList<>();

    public List<Map<String, String>> getMetadata() {
        return metadata;
    }

    public void setMetadata(String status, String code, String message) {
        Map<String, String> map = new HashMap<>();
        map.put("status", status);
        map.put("code", code);
        map.put("message", message);

        metadata.add(map);
    }
}
