package com.diego.demo.config;

import javax.sql.DataSource;   

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
 
@Configuration
public class ConfigSecurity {

    @Bean
    public  UserDetailsManager userDetailsManager(DataSource datasource) {
        return new JdbcUserDetailsManager(datasource);
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.authorizeHttpRequests(configure -> {
            configure.requestMatchers(HttpMethod.GET, "/trivial-app/preguntas").hasAnyRole("USER","ADMIN")
                    .requestMatchers(HttpMethod.GET, "/trivial-app/preguntas/**").hasAnyRole("USER","ADMIN")
                    .requestMatchers(HttpMethod.POST, "/trivial-app/preguntas").hasRole("ADMIN")
                    .requestMatchers(HttpMethod.PUT, "/trivial-app/preguntas/**").hasRole("ADMIN")
                    .requestMatchers(HttpMethod.DELETE, "/trivial-app/preguntas/**").hasRole("ADMIN")
                    .requestMatchers(HttpMethod.GET, "/swagger-ui/**", "/v3/api-docs/**", "/swagger-ui.html", "/webjars/**").permitAll()
                    .anyRequest().authenticated();
        });
        http.httpBasic(Customizer.withDefaults());
        http.csrf(AbstractHttpConfigurer::disable);
        return http.build();
    }
} 