package com.diego.demo.servicio;
 
import java.util.ArrayList;         

import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.diego.demo.modelo.Pregunta;
import com.diego.demo.modelo.dao.IPreguntaDao;
import com.diego.demo.response.PreguntaResponseRest;

@Service
public class PreguntaServicioImpl implements IPreguntaServicio {

    private static final Logger log = LoggerFactory.getLogger(PreguntaServicioImpl.class);

    @Autowired
    private IPreguntaDao preguntaDao;

    @Override
    @Transactional(readOnly = true)
    public ResponseEntity<PreguntaResponseRest> obtenerPreguntas() {
        log.info("Inicio método obtenerPreguntas()");

        PreguntaResponseRest response = new PreguntaResponseRest();

        try {
            List<Pregunta> preguntas = (List<Pregunta>) preguntaDao.findAll();

            response.getPreguntaResponse().setCategoria(preguntas);

            response.setMetadata("Respuesta ok", "00", "Respuesta exitosa");
        } catch (Exception e) {
            response.setMetadata("Respuesta nok", "-1", "Error al consultar preguntas");
            log.error("Error al consultar preguntas: ", e.getMessage());
            e.printStackTrace();
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    @Transactional(readOnly = true)
    public ResponseEntity<PreguntaResponseRest> obtenerPreguntaPorId(Long id) {
        log.info("Inicio método obtenerPreguntaPorId)");

        PreguntaResponseRest response = new PreguntaResponseRest();
        List<Pregunta> lista = new ArrayList<>();

        try {
            Optional<Pregunta> pregunta = preguntaDao.findById((long) id);

            if (pregunta.isPresent()) {
                lista.add(pregunta.get());
                response.getPreguntaResponse().setCategoria(lista);
            } else {
                log.error("Error en consultar pregunta");
                response.setMetadata("Respuesta nok", "-1", "Pregunta no encontrada");
                return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            log.error("Error en consultar pregunta");
            response.setMetadata("Respuesta nok", "-1", "Error al consultar pregunta");
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        response.setMetadata("Respuesta ok", "00", "Respuesta exitosa");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    @Transactional
    public ResponseEntity<PreguntaResponseRest> crearPregunta(Pregunta pregunta) {
        log.info("Inicio método crearPregunta");

        PreguntaResponseRest response = new PreguntaResponseRest();
        List<Pregunta> lista = new ArrayList<>();

        try {
            Pregunta preguntaGuardada = preguntaDao.save(pregunta);

            if (preguntaGuardada != null) {
                lista.add(preguntaGuardada);
                response.getPreguntaResponse().setCategoria(lista);
            } else {
                log.error("Error en guardar pregunta");
                response.setMetadata("Respuesta nok", "-1", "Pregunta no guardada");
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }

        } catch (Exception e) {
            log.error("Error en guardar pregunta");
            response.setMetadata("Respuesta nok", "-1", "Error al guardar pregunta");
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        response.setMetadata("Respuesta ok", "00", "Pregunta creada");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    
    @Override
    @Transactional
    public ResponseEntity<PreguntaResponseRest> actualizarPregunta(Pregunta pregunta, Long id) {
        PreguntaResponseRest response = new PreguntaResponseRest();
        List<Pregunta> lista = new ArrayList<>();
        try {
            Optional<Pregunta> preguntaBuscada = preguntaDao.findById((long) id);
            if (preguntaBuscada.isPresent()) {
                Pregunta preguntaActualizar = preguntaBuscada.get();
                preguntaActualizar.setContent(pregunta.getContent());
                preguntaActualizar.setCategory(pregunta.getCategory());
                // Actualizar otros campos según tu modelo
                Pregunta preguntaActualizada = preguntaDao.save(preguntaActualizar);
                if (preguntaActualizada != null) {
                    lista.add(preguntaActualizada);
                    response.getPreguntaResponse().setCategoria(lista);
                    response.setMetadata("OK", "00", "Pregunta actualizada exitosamente");
                } else {
                    response.setMetadata("ERROR", "-1", "Error al actualizar la pregunta");
                    return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
                }
            } else {
                response.setMetadata("ERROR", "-1", "Pregunta no encontrada");
                return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            response.setMetadata("ERROR", "-1", "Error al actualizar la pregunta");
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    @Transactional
    public ResponseEntity<PreguntaResponseRest> eliminarPregunta(Long id) {
        PreguntaResponseRest response = new PreguntaResponseRest();
        try {
            preguntaDao.deleteById((long) id);
            response.setMetadata("OK", "00", "Pregunta eliminada exitosamente");
        } catch (Exception e) {
            response.setMetadata("ERROR", "-1", "Error al eliminar la pregunta");
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    
    @Override
    @Transactional(readOnly = true)
    public ResponseEntity<PreguntaResponseRest> obtenerPreguntaAleatoriaPorId() {
        log.info("Inicio método obtenerPreguntaAleatoriaPorId");

        PreguntaResponseRest response = new PreguntaResponseRest();
        List<Pregunta> lista = new ArrayList<>();

        try {
            List<Pregunta> todasLasPreguntas = (List<Pregunta>) preguntaDao.findAll();

            if (todasLasPreguntas.isEmpty()) {
                response.setMetadata("Respuesta nok", "-1", "No hay preguntas disponibles");
                return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
            }

            Random random = new Random();
            int indiceAleatorio = random.nextInt(todasLasPreguntas.size());
            Pregunta preguntaAleatoria = todasLasPreguntas.get(indiceAleatorio);

            lista.add(preguntaAleatoria);
            response.getPreguntaResponse().setCategoria(lista);

        } catch (Exception e) {
            log.error("Error al obtener pregunta aleatoria por ID");
            response.setMetadata("Respuesta nok", "-1", "Error al obtener pregunta aleatoria por ID");
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        response.setMetadata("Respuesta ok", "00", "Pregunta aleatoria por ID obtenida exitosamente");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    @Transactional(readOnly = true)
    public ResponseEntity<PreguntaResponseRest> obtenerPreguntaAleatoriaPorCategoria(String category) {
        log.info("Inicio método obtenerPreguntaAleatoriaPorCategoria");

        PreguntaResponseRest response = new PreguntaResponseRest();
        List<Pregunta> lista = new ArrayList<>();

        try {
            List<Pregunta> preguntasPorCategoria = preguntaDao.findByCategory(category);

            if (preguntasPorCategoria.isEmpty()) {
                response.setMetadata("Respuesta nok", "-1", "No hay preguntas disponibles para la categoría especificada");
                return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
            }

            Random random = new Random();
            int indiceAleatorio = random.nextInt(preguntasPorCategoria.size());
            Pregunta preguntaAleatoria = preguntasPorCategoria.get(indiceAleatorio);

            lista.add(preguntaAleatoria);
            response.getPreguntaResponse().setCategoria(lista);

        } catch (Exception e) {
            log.error("Error al obtener pregunta aleatoria por categoría");
            response.setMetadata("Respuesta nok", "-1", "Error al obtener pregunta aleatoria por categoría");
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        response.setMetadata("Respuesta ok", "00", "Pregunta aleatoria por categoría obtenida exitosamente");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}
