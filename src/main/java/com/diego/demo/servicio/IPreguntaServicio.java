package com.diego.demo.servicio;


import org.springframework.http.ResponseEntity;   

import com.diego.demo.modelo.Pregunta;
import com.diego.demo.response.PreguntaResponseRest;

public interface IPreguntaServicio {

    ResponseEntity<PreguntaResponseRest> obtenerPreguntas();
    ResponseEntity<PreguntaResponseRest> obtenerPreguntaPorId(Long id);
    ResponseEntity<PreguntaResponseRest> crearPregunta(Pregunta pregunta);
    ResponseEntity<PreguntaResponseRest> actualizarPregunta(Pregunta pregunta, Long id);
    ResponseEntity<PreguntaResponseRest> eliminarPregunta(Long id);
	ResponseEntity<PreguntaResponseRest> obtenerPreguntaAleatoriaPorId();
	ResponseEntity<PreguntaResponseRest> obtenerPreguntaAleatoriaPorCategoria(String category);

}