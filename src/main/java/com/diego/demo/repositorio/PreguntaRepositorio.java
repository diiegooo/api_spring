package com.diego.demo.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;  

import com.diego.demo.modelo.Pregunta;

public interface PreguntaRepositorio extends JpaRepository<Pregunta, Long> {
}