package com.diego.demo.controlador;

    
import org.springframework.beans.factory.annotation.Autowired;  
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.diego.demo.modelo.Pregunta;
import com.diego.demo.response.PreguntaResponseRest;
import com.diego.demo.servicio.IPreguntaServicio;

import io.swagger.v3.oas.annotations.parameters.RequestBody;

@RestController
@RequestMapping("/trivial-app")
public class PreguntaRestController {

	@Autowired
    private IPreguntaServicio service;

    @GetMapping("/preguntas")
    public ResponseEntity<PreguntaResponseRest> obtenerPreguntas() {
        ResponseEntity<PreguntaResponseRest> response = service.obtenerPreguntas();
        return response;
    }

    @GetMapping("/preguntas/{id}")
    public ResponseEntity<PreguntaResponseRest> obtenerPreguntaPorId(@PathVariable Long id) {
        ResponseEntity<PreguntaResponseRest> response = service.obtenerPreguntaPorId(id);
        return response;
    }

    @PostMapping("/preguntas")
    public ResponseEntity<PreguntaResponseRest> crearPregunta(@RequestBody Pregunta request) {
        ResponseEntity<PreguntaResponseRest> response = service.crearPregunta(request);
        return response;
    }

    @PutMapping("/preguntas/{id}")
    public ResponseEntity<PreguntaResponseRest> actualizarPregunta(@RequestBody Pregunta request, @PathVariable Long id) {
        ResponseEntity<PreguntaResponseRest> response = service.actualizarPregunta(request, id);
        return response;
    }

    @DeleteMapping("/preguntas/{id}")
    public ResponseEntity<PreguntaResponseRest> eliminarPregunta(@PathVariable Long id) {
        ResponseEntity<PreguntaResponseRest> response = service.eliminarPregunta(id);
        return response;
    }
    
    @GetMapping("/preguntas/random/id")
    public ResponseEntity<PreguntaResponseRest> obtenerPreguntaAleatoriaPorId() {
        ResponseEntity<PreguntaResponseRest> response = service.obtenerPreguntaAleatoriaPorId();
        return response;
    }

    @GetMapping("/preguntas/random/categoria/{categoria}")
    public ResponseEntity<PreguntaResponseRest> obtenerPreguntaAleatoriaPorCategoria(@PathVariable String categoria) {
        ResponseEntity<PreguntaResponseRest> response = service.obtenerPreguntaAleatoriaPorCategoria(categoria);
        return response;
    }
    
}