package com.diego.demo.modelo.dao;

import java.util.List;     

import org.springframework.data.repository.CrudRepository;

import com.diego.demo.modelo.Pregunta;

//IPreguntaDao.java
public interface IPreguntaDao extends CrudRepository<Pregunta, Long> {
 List<Pregunta> findByCategory(String category);
}