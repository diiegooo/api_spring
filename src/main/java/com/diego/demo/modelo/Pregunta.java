package com.diego.demo.modelo;

import jakarta.persistence.Entity;   

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

//Pregunta.java
@Entity
public class Pregunta {
	
 @Id
 @GeneratedValue(strategy = GenerationType.IDENTITY)
 
 private Long id;
 private String content;
 private String category;
 
public Long getId() {
	return id;
}

public void setId(Long id) {
	this.id = id;
}

public String getContent() {
	return content;
}

public void setContent(String content) {
	this.content = content;
}

public String getCategory() {
	return category;
}

public void setCategory(String category) {
	this.category = category;
}

public Pregunta(Long id, String content, String category) {
	super();
	this.id = id;
	this.content = content;
	this.category = category;
}

public Pregunta(String content, String category) {
	super();
	this.content = content;
	this.category = category;
}

public Pregunta() {
	super();
}


}
