package com.diego.demo.service;

import com.diego.demo.modelo.Pregunta; 
import com.diego.demo.modelo.dao.IPreguntaDao;
import com.diego.demo.response.PreguntaResponseRest;
import com.diego.demo.servicio.PreguntaServicioImpl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
 
public class PreguntaServiceImplTest {

    @InjectMocks
    PreguntaServicioImpl service;

    @Mock
    IPreguntaDao preguntaDao;

    List<Pregunta> listaPreguntas;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        this.listaPreguntas = new ArrayList<>();
        this.cargarPreguntas();
    }

    @Test
    public void obtenerPreguntasTest() {
        when(preguntaDao.findAll()).thenReturn(listaPreguntas);

        ResponseEntity<PreguntaResponseRest> response = service.obtenerPreguntas();

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(4, response.getBody().getPreguntaResponse().getCategoria().size());
    }

    @Test
    public void obtenerPreguntaPorIdExistenteTest() {
        Long idExistente = 1L;
        Pregunta preguntaExistente = new Pregunta();
        preguntaExistente.setId(idExistente);
        when(preguntaDao.findById(idExistente)).thenReturn(Optional.of(preguntaExistente));

        ResponseEntity<PreguntaResponseRest> response = service.obtenerPreguntaPorId(idExistente);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(1, response.getBody().getPreguntaResponse().getCategoria().size());
    }

    @Test
    public void obtenerPreguntaPorIdNoExistenteTest() {
        Long idNoExistente = 999L;
        when(preguntaDao.findById(idNoExistente)).thenReturn(Optional.empty());

        ResponseEntity<PreguntaResponseRest> response = service.obtenerPreguntaPorId(idNoExistente);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void crearPreguntaTest() {
        Pregunta nuevaPregunta = new Pregunta();
        when(preguntaDao.save(any(Pregunta.class))).thenReturn(nuevaPregunta);

        ResponseEntity<PreguntaResponseRest> response = service.crearPregunta(nuevaPregunta);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(1, response.getBody().getPreguntaResponse().getCategoria().size());
    }

    @Test
    public void actualizarPreguntaExistenteTest() {
        Long idExistente = 1L;
        Pregunta preguntaExistente = new Pregunta();
        preguntaExistente.setId(idExistente);
        when(preguntaDao.findById(idExistente)).thenReturn(Optional.of(preguntaExistente));
        when(preguntaDao.save(any(Pregunta.class))).thenReturn(preguntaExistente);

        ResponseEntity<PreguntaResponseRest> response = service.actualizarPregunta(new Pregunta(), idExistente);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(1, response.getBody().getPreguntaResponse().getCategoria().size());
    }

    @Test
    public void actualizarPreguntaNoExistenteTest() {
        Long idNoExistente = 999L;
        when(preguntaDao.findById(idNoExistente)).thenReturn(Optional.empty());

        ResponseEntity<PreguntaResponseRest> response = service.actualizarPregunta(new Pregunta(), idNoExistente);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void eliminarPreguntaExistenteTest() {
        Long idExistente = 1L;
        doNothing().when(preguntaDao).deleteById(idExistente);

        ResponseEntity<PreguntaResponseRest> response = service.eliminarPregunta(idExistente);

        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void eliminarPreguntaNoExistenteTest() {
        Long idNoExistente = 999L;
        doThrow(new RuntimeException()).when(preguntaDao).deleteById(idNoExistente);

        ResponseEntity<PreguntaResponseRest> response = service.eliminarPregunta(idNoExistente);

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
    }

    private void cargarPreguntas() {
        for (int i = 1; i <= 4; i++) {
            Pregunta pregunta = new Pregunta();
            pregunta.setId((long) i);
            listaPreguntas.add(pregunta);
        }
    }
}
