package com.diego.demo.controllers;

import com.diego.demo.controlador.PreguntaRestController;  
import com.diego.demo.modelo.Pregunta;
import com.diego.demo.response.PreguntaResponseRest;
import com.diego.demo.servicio.IPreguntaServicio;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class PreguntaRestControllerTest {

    @InjectMocks
    PreguntaRestController preguntaController;

    @Mock
    private IPreguntaServicio service;

    @BeforeEach
    public void init(){
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void obtenerPreguntasTest() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));


        when(service.obtenerPreguntas()).thenReturn(new ResponseEntity<>(new PreguntaResponseRest(), HttpStatus.OK));

        ResponseEntity<PreguntaResponseRest> respuesta = preguntaController.obtenerPreguntas();

        assertThat(respuesta.getStatusCodeValue()).isEqualTo(200);
    }

    @Test
    public void obtenerPreguntaPorIdTest() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        long id = 1;

        when(service.obtenerPreguntaPorId(id)).thenReturn(new ResponseEntity<>(new PreguntaResponseRest(), HttpStatus.OK));

        ResponseEntity<PreguntaResponseRest> respuesta = preguntaController.obtenerPreguntaPorId(id);

        assertThat(respuesta.getStatusCodeValue()).isEqualTo(200);
    }

    @Test
    public void crearPreguntaTest() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        Pregunta pregunta = new Pregunta();

        when(service.crearPregunta(any(Pregunta.class))).thenReturn(new ResponseEntity<>(HttpStatus.OK));

        ResponseEntity<PreguntaResponseRest> respuesta = preguntaController.crearPregunta(pregunta);

        assertThat(respuesta.getStatusCodeValue()).isEqualTo(200);
    }

    @Test
    public void actualizarPreguntaTest() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        long id = 1;
        Pregunta pregunta = new Pregunta();

        when(service.actualizarPregunta(any(Pregunta.class), any(Long.class)))
                .thenReturn(new ResponseEntity<>(new PreguntaResponseRest(), HttpStatus.OK));

        ResponseEntity<PreguntaResponseRest> respuesta = preguntaController.actualizarPregunta(pregunta, id);

        assertThat(respuesta.getStatusCodeValue()).isEqualTo(200);
    }

	@Test
    public void eliminarPreguntaTest() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        long id = 1;

        when(service.eliminarPregunta(id)).thenReturn(new ResponseEntity<>(new PreguntaResponseRest(), HttpStatus.OK));

        ResponseEntity<PreguntaResponseRest> respuesta = preguntaController.eliminarPregunta(id);

        assertThat(respuesta.getStatusCodeValue()).isEqualTo(200);
    }
	
	@Test
    public void obtenerPreguntaAleatoriaPorIdTest() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        when(service.obtenerPreguntaAleatoriaPorId()).thenReturn(new ResponseEntity<>(new PreguntaResponseRest(), HttpStatus.OK));

        ResponseEntity<PreguntaResponseRest> respuesta = preguntaController.obtenerPreguntaAleatoriaPorId();

        assertThat(respuesta.getStatusCodeValue()).isEqualTo(200);
    }

    @Test
    public void obtenerPreguntaAleatoriaPorCategoriaTest() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        String categoria = "Ciencia";

        when(service.obtenerPreguntaAleatoriaPorCategoria(categoria))
                .thenReturn(new ResponseEntity<>(new PreguntaResponseRest(), HttpStatus.OK));

        ResponseEntity<PreguntaResponseRest> respuesta = preguntaController.obtenerPreguntaAleatoriaPorCategoria(categoria);

        assertThat(respuesta.getStatusCodeValue()).isEqualTo(200);
    }
}